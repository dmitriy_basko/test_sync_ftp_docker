Create 3 docker containers. All files must be commited to a git repository. You can use any git service like bitbucket, github or gitlab.

The first container will run lsyncd (https://github.com/axkibe/lsyncd) to sync all files from his mounted directory on /data to the target server(s). When changes occour in the source directory, lsyncd on the source container has to sync all changes to both of the target containers.

Create the lsyncd container from scratch. You can use alpine (preferred) or ubuntu as base image. The Dockerfile and all other files that belong to this container have to be in the folder lsyncd.

The second and third containers will act as a sftp servers and will recieve files from lsyncd via sftp. For the target containers you can use the atmoz/sftp or another image or even build it from scratch.


Example docker-compose.yml:
   
```dockerfile
version: '3'
services:
 lsyncd:
   build: lsyncd/
   volumes:
     - ./source-data:/data
   environment:
     - SYNC_TARGETS="target1,target2"
 target1:
   image: atmoz/sftp
   volumes:
     - ./target-data-1:/home/user1/data
   command: user1:password1
 target2:
   image: atmoz/sftp
   volumes:
     - ./target-data-2:/home/user2/data
   command: user2:password2
```